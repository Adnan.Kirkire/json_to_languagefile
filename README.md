# json-to-languageList

### Overview
Generates .c files with language specification based on an input json file with all language information

### Dependencies
Go to repo directory and install packages using: 
npm install

### Execution
To run with node:
node json_to_languages.js [input .json file] [output directory]

to generate windows executable run:
pkg json_to_languages.js -t node14-win-x64

to run executable:
json_to_languages.exe [input .json file] [output directory]


### To add additional menu items
The **hmi_strings.json** file is the master file containing all menu items for gxt5. If additional items are needed in the menu, the **hmi_strings.json** file must me modified to maintain version control. 

1. Modify .json file:
    
1.1 Add new menu item to "strings" key in the order specified in the "languages" key:
        
![New menu item](source/images/strings.jpg)
       

![Languages key for reference](source/images/languages.jpg)
        

1.2 Add an ID for the new menu item to the "enum_ids" key (keeping the order of items in the "strings" key):

![IDs](source/images/Ids.jpg)

2. Run code with new .json file to generate new language files. 
