#!/usr/bin/env node
/***********************************************************
* json_to_languages.js
* Author: Anelise Chapa 
************************************************************
* Description: Generates language files for display 
* descriptionsbased on input languages.json file. 
* Separates json into separate
* language tables and outputs those into files named as
* language.
* 
************************************************************/

/***********************************************************
* Constant definitions
************************************************************/

//node packages
const fs = require('fs');
const encoding = require("encoding");
const { stringify } = require('querystring');
const { exit } = require('process');

//file to output text enumerations
const txtIds_file = "text_ids.h"

//constant values for output text in language files.
const newline_file_division = "//------------------------------------------------------------------------------\n"
const initial_lang_file_chars = 
"#include <stdlib.h>  //wchar_t\n"+
"#include <commonservice.h>\n\n" + newline_file_division +
'#pragma default_variable_attributes = @ "languagesection"\n\n'+
"const wchar_t * const g_"
const initial_lang_table_chars = '_TextTable [] = \n{\n#ifdef MUTI_LANGUAGE_SUPPORT\n'
const final_lang_file_chars = "#else\n"+"NULL\n"+"#endif\n\n"+"};\n" + newline_file_division
const initial_textids_chars = newline_file_division +
"#ifndef _TEXTIDS_H__\n"+"#define __TEXTIDS_H__\n"+ newline_file_division +
"#include <stdlib.h>\n" + newline_file_division
const initial_enum_chars = "typedef enum\n{\n"
const txt_table_def_chars = "\n#define TEXT_ID_TO_WSTR(TextID)      DrawProp.TextTable[TextID]\n" + 
                            newline_file_division
const txtids_final_chars = newline_file_division+ '\n' +newline_file_division+"#endif // #ifndef __TEXTIDS_H__"
const txtids_languageTables_chars_start = "extern const wchar_t const *g_"
const txtids_languageTables_chars_end = "_TextTable [];\n"
const numberOfLanguages = 10

/***********************************************************
* function definitions
************************************************************/

/************************************************************
 * @brief inputs a string and returns that string as a unicode 
 * hex enclosed in ''. Exceptions are empty strings and nulls
 * 
 * @param charCode file pointer to string value
 ************************************************************/

function getCharacterAsHexString(charCode) 
{
    //null format as hex string
    if(charCode == null){
        return '    '+"NULL"
    }
    if(charCode == ""){
        return '    L""'
    }
    var arr = [];
    for (var i = 0; i < charCode.length; i++) {
        let str = String.fromCharCode(charCode.charCodeAt(i))
        arr[i] = ("00" + charCode.charCodeAt(i).toString(16)).slice(-4)
    }
    return ('    L"\\x' + arr.join('\\x') + '"')
}

/************************************************************
 * @brief 
 * 
 * @param charCode file pointer
 ************************************************************/

async function writeToFile(filepath, initial_string) {
    return new Promise((resolve, reject) => {
      fs.writeFile(filepath, initial_string, (e) => {
        if (e) {
          errorHandler(e);
          // if you want to continue and adjust the count 
          // even if the writeFile failed
          // you don't need the reject here
          return reject(e);
        }
        resolve();
      });
    });
}

/************************************************************
 * @brief Main Program
 * 
 * @param None
 ************************************************************/
var myArgs = process.argv.slice(2);
let input_file = myArgs[0]          //user input: json file
let output_directory = myArgs[1]    //user input: output directory

//Add / at end of output directory if not included in input
if(output_directory[output_directory.length - 1] != '/'){
    output_directory+='/'
}

//Read the json file and parse it to (key, value) objects
const file_contents = fs.readFileSync(input_file)
let json_file = JSON.parse(file_contents.toString())
let txt_ids = Object.keys(json_file)
let language_descriptions = []
let all_strings = []
let enum_ids = []

let output_str_to_language_files = []
//let output_str_to_txt_ids_file = initial_textids_chars + initial_enum_chars

//Search for languages
for(let i = 0; i < txt_ids.length; i++) {
    if(txt_ids[i] == "languages") {
        language_descriptions = Object.values(json_file[txt_ids[i]])
    }
    if(txt_ids[i] == "strings") {
        all_strings = Object.values(json_file[txt_ids[i]])
    }
    if(txt_ids[i] == "enum_ids") {
        enum_ids = Object.values(json_file[txt_ids[i]])
    }
}

if(language_descriptions == [] || all_strings == []) {
    console.log("ERROR READING JSON FILE")
    exit();
}
console.log('\nGenerating language files for: ')
for(let i = 0; i < language_descriptions.length; i++) {
    output_str_to_language_files.push("") //list of languages to hold array of descriptions
    
}

//output_str_to_txt_ids_file += '    LANG_LAST_LANGUAGE\n}LangID_t;\n\n' + initial_enum_chars

//For all keys found in files, append to corresponding language array.
for(let i  = 0; i <all_strings.length; i++) {
    ///output_str_to_txt_ids_file+= '    '+ txt_id+' = '+i+',\n'
    for(let j = 0; j < language_descriptions.length; j++) {
        //add each character as unicode hex
        recode_string = getCharacterAsHexString(all_strings[i][j])   
        //add a description - string text - in the form of a comment
        recode_string += ', /* '+ enum_ids[i] + ' -- ' + all_strings[i][j]+' */\n'
        output_str_to_language_files[j] += recode_string
        //json_file[txt_id][val] = recode_string
    }
}  

//output_str_to_txt_ids_file += '\n}TextID_t;\n\n'+txt_table_def_chars
//add final characters to language file
for(let i = 0; i < language_descriptions.length; i++) {   
    console.log(language_descriptions[i])
    output_str_to_language_files[i] += final_lang_file_chars
    let output_language_file = output_directory +'Language_'+language_descriptions[i] + '.c'
    writeToFile(output_language_file, initial_lang_file_chars+
                                      language_descriptions[i]+
                                      initial_lang_table_chars+ 
                                      output_str_to_language_files[i])
    //output_str_to_txt_ids_file +=  txtids_languageTables_chars_start + language_descriptions[i]+txtids_languageTables_chars_end
}

//output_str_to_txt_ids_file += txtids_final_chars
//writeToFile(output_directory + txtIds_file,output_str_to_txt_ids_file)
console.log('\nSuccess!');



/***********************************************************
* EOF
************************************************************/